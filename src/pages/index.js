import React from 'react'

import Layout from '../components/layout'

const IndexPage = () => (
  <Layout>
    <h2>We're building an Open Source platform for issuing and exchanging Fiscal Money</h2>

    <p>A fiscal money unit is a tax rebate certificate issued by a tax imposing governement. Fiscal Money can allow non monetarily sovereign governments, like municipalities, states, and provinces to exercise some of the levers provided by monetary sovereingty.</p>

    <p>Example: a municipality hires Bob to mow the lawn in front of the town hall, and pays him with certificate redeemable for a $100 rebate in property tax. Anyone in the city having a yearly tax bill (perhaps Bob himself) will accept the $100 rebate as real money.</p>   

    <p>The goal of the Chartalis project is to provide a turnkey platform for issuing and exchanging tax credits. Tax credits are issued and exchanged on 
      the <a href='http://stellar.org' target="blank">Stellar</a> network    
      from a web or smartphone application.</p>
    
    <p><a href='http://stellar.org' target="blank">Stellar</a> is a public transaction network supported by a non profit organization. 
     The network consists of nodes that maintain a ledger via a <a href="https://www.stellar.org/developers/guides/concepts/scp.html" target="blank">consensus protocol</a> over the web. Anyone can run a stellar node at no 
     cost in the same way anyone can host an email server and take part in the global email network. Non proprietary protocols such 
     as Stellar and email leave no opportunity for rent seeking, as a result transaction costs are driven near their computing costs: sending an 
     email costs a tiny fraction of a penny, a stellar transaction costs 1/4000 th of a penny at the time of writing these lines.</p> 
    
     <h2>A side effect of Fiscal Money: disseminate understanding of Modern Monetary Theory</h2>

     <p>Notice how the following statements are easily understandable and uncontroversial (they are core propositions of MMT where the word “money unit” has been replaced with “tax credit”):</p>

     <ul>
       <li>A Tax Credit is spent into existence, and taxed out of existence</li>
       <li>Tax Credits must be spent before they can be taxed: Spending precedes taxation</li>
       <li>A Tax Credit is valuable because there is a tax.</li>
       <li>Savings in Tax Credits are equal to the amount of credits spent that haven’t been taxed.</li>
       <li>A Tax Credit issuer never needs to borrow Tax Credits. It can however, convert credits into interest earning credits, to incite their owners to hold on to them longer than they otherwise would.</li>
     </ul>

     <p>Anyone who undestands the concept of a rebate certificate can understand these statements and recognize them as truisms. The same phrases using the word “money” triggers the cognitive dissonance typical of the first exposure to MMT because it clashes with the misleading metaphor of “money as a ressource” that is unfortunately the prevalent mental model of money. Owning and exchanging fiscal money should cause reflection on the nature of money, and lead to the realization that “real” money is in fact fiscal money ! That is in essence the chartalist thesis: a money unit is a tax credit emitted by an institution with taxing power. </p>
 
     <h2>Wanted: Municipalities wanting to issue Fiscal Money</h2>

     <p>The Chartalis project is in alpha stage. Recruting of a first tax imposing governement would be essential at this stage, to obtain critical design input and eventualy to issue a first batch of fiscal money ! The earlier the input of real users in the design of a software system, the higher the quality.</p>

    <p>Contact:  <a href="mailto:info@chartalis.org">info at chartalis.org</a>  </p>

    <p>Gitlab: <a href="https://gitlab.com/chartalis">https://gitlab.com/chartalis</a></p>

    <a href="https://twitter.com/chartalis?ref_src=twsrc%5Etfw" 
      className={"twitter-follow-button"} 
      data-size="large" 
      data-show-count="false">Follow @chartalis</a>
      
  </Layout>
)

export default IndexPage
